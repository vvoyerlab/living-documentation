﻿
using LivingDocumentation.Renderers;

namespace LivingDocumentation.FeatureTests.MkDocs;

internal static class Dataset
{
    public readonly static Documentation Documentation = new Documentation()
    {
        Glossary = new LivingGlossay([
                new Word(Name: "A Word", Definition: "Awsome definition")
                ]),
        Features = new[]
            {
                new LivingFeature(
                    Name: "Awsome feature",
                    Description: "It's an awsome feature",
                    Links: ["https://link.to"],
                    Theme: "Awsome"
                    ),
                new LivingFeature(
                    Name: "Another feature",
                    Description: "Another feature",
                    Links: Array.Empty<string>(),
                    Theme: null
                    ),
            },
        Onboarding = new LivingOnboarding([
                new Step(
                    Rank: 0,
                    Title: "Base id the Base",
                    Brief: "Base of an awsome class",
                    PathToFile: "go/to/source/Base.cs"
                    ),
                new Step(
                    Rank: 1,
                    Title: "Awsome code",
                    Brief: "Awsome implementation of an incredible base",
                    PathToFile: "go/to/source/Implementation.cs"
                    ),
                ]),
        Diagram = new LivingDiagram(
                Elements: [
                    new Element(
                        Name: "Base",
                        Namespace: "",
                        Type: ElementType.Class
                        ),
                    new Element(
                        Name: "Implementation",
                        Namespace: "",
                        Type: ElementType.Class
                        ),
                    new Element(
                        Name: "State",
                        Namespace: "",
                        Type: ElementType.Enum
                        ),
                    ],
                Relationships: [
                    new Relationship
                    {
                        ElementNameFrom = "Base",
                        ElementNameTo = "State",
                        Type = RelationshipType.RelatedTo,
                        Name = "",
                    },
                    new Relationship
                    {
                        ElementNameFrom = "Implementation",
                        ElementNameTo = "Base",
                        Type = RelationshipType.Extends,
                    },
                    ]
                ),
        Decisions = [
                new LivingDecision
                {
                    Title = "An important decision",
                    Description = "An important decision for the application",
                    Status = nameof(AnyDecisionRecord.DecisionStatus.Accepted),
                    StartDate = "2024-04-01",
                    ReleaseDate = "2024-04-01",
                    ProblemsToSolve = "A problem to solve",
                    ConsideredOptions = ["Awsome option","Another option"],
                    DecisionOutcome = "Awsome option",
                    DecisionOutcomeJustification = "Because it's awsome",
                }
                ]
    };
}
