﻿using LivingDocumentation.Renderers;
using LivingDocumentation.Renderers.Mkdocs;
using System.IO.Abstractions.TestingHelpers;

namespace LivingDocumentation.FeatureTests.MkDocs;

public class MkDocsRendererFeatureTest
{
    [Fact]
    public async Task MkDocs_Rendering()
    {
        IDocumentationConfiguration documentationConfiguration = new FakeDocumenationConfiguration();
        string featuresFolder = Path.Combine(documentationConfiguration.DocumentationFolder, "docs", "features");
        string decisionsFolder = Path.Combine(documentationConfiguration.DocumentationFolder, "docs", "decisions");
        string glossaryFileName = Path.Combine(documentationConfiguration.DocumentationFolder, "docs", "glossary.md");
        string onboardingFileName = Path.Combine(documentationConfiguration.DocumentationFolder, "docs", "onboarding.md");
        string diagramFileName = Path.Combine(documentationConfiguration.DocumentationFolder, "docs", "diagram.md");
        string mkdocConfigFile = Path.Combine(documentationConfiguration.DocumentationFolder, "mkdocs.yml");
        string mkdocDependencies = Path.Combine(documentationConfiguration.DocumentationFolder, "requirements.txt");
        MockFileSystem fileSystem = new MockFileSystem();
        fileSystem.AddEmptyFile("README.md");
        MkDocsConfiguration mkDocsConfiguration = new MkDocsConfiguration()
        {
            FileSystem = fileSystem,
            DiagramRenderer = MkDocsConfiguration.DiagramProvider.Mermaid
        };        
        DocumentationRenderer mkDocs = new MkDocsRenderer(documentationConfiguration, mkDocsConfiguration);
        
        ErrorOr<Success> result = await mkDocs.Render(Dataset.Documentation);

        result.IsError.Should().BeFalse();
        fileSystem.Directory.Exists(documentationConfiguration.DocumentationFolder).Should().BeTrue();
        fileSystem.File.Exists(mkdocConfigFile).Should().BeTrue();
        fileSystem.File.Exists(mkdocDependencies).Should().BeTrue();
        fileSystem.File.Exists(glossaryFileName).Should().BeTrue();
        fileSystem.File.Exists(onboardingFileName).Should().BeTrue();
        fileSystem.File.Exists(diagramFileName).Should().BeTrue();
        fileSystem.Directory.Exists(featuresFolder).Should().BeTrue();
        fileSystem.Directory.GetFiles(featuresFolder, "*.md").Length.Should().Be(Dataset.Documentation.Features.Length);
        fileSystem.Directory.Exists(decisionsFolder).Should().BeTrue();
        fileSystem.Directory.GetFiles(decisionsFolder, "*.md").Length.Should().Be(Dataset.Documentation.Decisions.Length);
    }
}
