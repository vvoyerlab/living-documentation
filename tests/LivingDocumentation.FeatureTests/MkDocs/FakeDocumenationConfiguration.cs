﻿namespace LivingDocumentation.FeatureTests.MkDocs
{
    class FakeDocumenationConfiguration
        : IDocumentationConfiguration
    {
        public string DocumentationFolder => "documentations";
        public string ProjectRelativePathFromOutputPath => throw new NotImplementedException();
        public string SourceRootPath => throw new NotImplementedException();
        public string ProjectRootPath => throw new NotImplementedException();
        public IEnumerable<Namespace> ExcludedNamespaces => throw new NotImplementedException();
        public string IndexFile => "README.md";
        public string DocumentationName => "Documentation";
        public string DocumentationUrl => "https://documentation.com";
        public string GlossaryName => "Glossary";
    }
}
