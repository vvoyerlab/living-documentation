﻿global using ErrorOr;
global using FluentAssertions;
global using LivingDocumentation.Annotation;
global using LivingDocumentation.Features.GenerateDecisionLog;
global using LivingDocumentation.Features.GenerateDiagram;
global using LivingDocumentation.Features.GenerateFeatures;
global using LivingDocumentation.Features.GenerateGlossary;
global using LivingDocumentation.Features.GenerateOnboarding;