﻿using LivingDocumentation.Features.GenerateDecisionLog;
using LivingDocumentation.Features.GenerateDiagram;
using LivingDocumentation.Features.GenerateFeatures;
using LivingDocumentation.Features.GenerateGlossary;
using LivingDocumentation.Renderers;
using LivingDocumentation.Renderers.Mkdocs;
using System.IO.Abstractions;
using Xunit.Abstractions;

namespace LivingDocumentation.Documentation;

public class DocumentationGenerator
{    
    private readonly ITestOutputHelper _output;
    private readonly Configuration _configuration = new Configuration();
    private readonly Namespace _namespace = "LivingDocumentation";

    public DocumentationGenerator(ITestOutputHelper output)
    {
        _output = output;
    }
    [Fact]
    public async Task Generate()
    {
        IGenerateGlossary generateGlossary = new LivingGlossaryGenerator(_configuration);
        IGenerateFeatures generateFeatures = new LivingFeatureGenerator(_configuration);
        IGenerateDiagram generateDiagram = new LivingDiagramGenerator(_configuration);
        IGenerateDecisionLog generateDecisionLog = new LivingDecisionLogGenerator(_configuration);
        
        ErrorOr<LivingGlossay> glossayResult = generateGlossary.Execute(_namespace);
        ErrorOr<LivingFeature[]> featuresResult = generateFeatures.Execute(_namespace);
        ErrorOr<LivingDecision[]> decisionsResult = generateDecisionLog.Execute(_namespace);

        List<Error> errors = new List<Error>();
        if (glossayResult.IsError) { errors.AddRange(glossayResult.Errors); }
        if (featuresResult.IsError) { errors.AddRange(featuresResult.Errors); }
        if (decisionsResult.IsError) { errors.AddRange(decisionsResult.Errors); }

        if (errors.Count > 0)
        {
            foreach (Error error in errors)
            {
                _output.WriteLine(error.Description);
                return;
            }
        }

        Renderers.Documentation documentation = new()
        {
            Glossary = glossayResult.Value,
            Features = featuresResult.Value,
            Decisions = decisionsResult.Value,
        };
        MkDocsConfiguration mkDocsConfiguration = new MkDocsConfiguration()
        {
            FileSystem = new FileSystem(),
            DiagramRenderer = MkDocsConfiguration.DiagramProvider.Mermaid,
        };
        DocumentationRenderer renderer = new MkDocsRenderer(_configuration, mkDocsConfiguration);

        ErrorOr<Success> renderingResult = await renderer.Render(documentation);
        if (renderingResult.IsError)
        {
            foreach (Error error in renderingResult.Errors)
            {
                _output.WriteLine(error.Description);
            }
        }
    }
}
