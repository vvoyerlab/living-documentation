using System.Reflection;

namespace LivingDocumentation.Documentation;

public class Configuration
    : IDocumentationConfiguration
{
    public string DocumentationFolder => Path.Combine(ProjectRootPath, "documentations");
    public string ProjectRelativePathFromOutputPath 
    { 
        get
        {            
            Uri outputPathUri = new Uri(DocumentationFolder + Path.DirectorySeparatorChar);
            Uri sourceRootPathUri = new Uri(SourceRootPath + Path.DirectorySeparatorChar);

            Uri relativeUri = outputPathUri.MakeRelativeUri(sourceRootPathUri);
            string relativePath = Uri.UnescapeDataString(relativeUri.ToString());
                        
            relativePath = relativePath.Replace('/', Path.DirectorySeparatorChar);

            return relativePath;
        }
    }
    public string SourceRootPath => Path.Combine(ProjectRootPath, "sources");
    public string ProjectRootPath
    {
        get
        {
            string assemblyPath = Assembly.GetExecutingAssembly().Location;
            string? directoryPath = Path.GetDirectoryName(assemblyPath);

            while (directoryPath != null && !Directory.EnumerateFiles(directoryPath, "*.sln").Any())
            {
                directoryPath = Directory.GetParent(directoryPath)?.FullName;
            }
            
            return directoryPath ?? throw new InvalidOperationException("Chemin de la racine du projet non trouv�.");
        }
    }
    public IEnumerable<Namespace> ExcludedNamespaces => ["LivingDocumentation.Documentation"];
    public string IndexFile => Path.Combine(ProjectRootPath,"README.md");
    public string DocumentationName => "Living Documentation";
    public string DocumentationUrl => "https://vvolab.gitlab.io/living-documentation";
    public string GlossaryName => "Annotations";
}