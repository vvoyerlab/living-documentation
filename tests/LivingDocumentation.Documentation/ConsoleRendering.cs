﻿using LivingDocumentation.Features.GenerateDecisionLog;
using LivingDocumentation.Features.GenerateDiagram;
using LivingDocumentation.Features.GenerateFeatures;
using LivingDocumentation.Features.GenerateGlossary;
using Xunit.Abstractions;

namespace LivingDocumentation.Documentation;

public class ConsoleRendering
{
    private const string SEPARATOR = "--------------------------------------";
    private readonly ITestOutputHelper _output;
    private readonly Configuration _configuration = new Configuration();
    private readonly Namespace _namespace = "LivingDocumentation";

    public ConsoleRendering(ITestOutputHelper output)
    {
        _output = output;
    }
    [Fact]
    public void GenerateGlossary()
    {
        _output.WriteLine("Annotations");

        IGenerateGlossary generateGlossary = new LivingGlossaryGenerator(_configuration);
        ErrorOr<LivingGlossay> glossayResult = generateGlossary.Execute(_namespace);

        if (glossayResult.IsError)
        {
            _output.WriteLine(glossayResult.FirstError.Description);
            return;
        }
        LivingGlossay glossay = glossayResult.Value;
        foreach (Word word in glossay.Words)
        {
            _output.WriteLine($"* {word.Name} : {word.Definition}");
        }
        _output.WriteLine(SEPARATOR);
    }

    [Fact]
    public void GenerateFeatures()
    {
        _output.WriteLine("Features");
        IGenerateFeatures generateFeatures = new LivingFeatureGenerator(_configuration);
        ErrorOr<LivingFeature[]> featuresResult = generateFeatures.Execute(_namespace);
        if (featuresResult.IsError)
        {
            _output.WriteLine(featuresResult.FirstError.Description);
            return;
        }
        LivingFeature[] features = featuresResult.Value;
        foreach (LivingFeature feature in features)
        {
            _output.WriteLine(feature.Name);
            _output.WriteLine(feature.Description);
            _output.WriteLine("---");
        }
        _output.WriteLine(SEPARATOR);
    }

    [Fact]
    public void GenerateDiagram()
    {
        _output.WriteLine("Diagram");
        IGenerateDiagram generateDiagram = new LivingDiagramGenerator(_configuration);
        _output.WriteLine(SEPARATOR);
    }

    [Fact]
    public void GenerateDecisionLog()
    {
        _output.WriteLine("Decisions");
        IGenerateDecisionLog generateDecisionLog = new LivingDecisionLogGenerator(_configuration);
        ErrorOr<LivingDecision[]> decisionsResult = generateDecisionLog.Execute(_namespace);
        if (decisionsResult.IsError)
        {
            _output.WriteLine(decisionsResult.FirstError.Description);
            return;
        }
        LivingDecision[] decisions = decisionsResult.Value;
        foreach (LivingDecision decision in decisions)
        {
            _output.WriteLine(decision.Title);
            _output.WriteLine(decision.Description);
            _output.WriteLine(decision.ProblemsToSolve);
            _output.WriteLine("Options :");
            foreach (string options in decision.ConsideredOptions)
                _output.WriteLine($"* {options}");
            _output.WriteLine("Options :");
            _output.WriteLine($"Decision Outcome :");
            _output.WriteLine(decision.DecisionOutcome);
            _output.WriteLine("---");
        }
        _output.WriteLine(SEPARATOR);
    }

}
