﻿
namespace LivingDocumentation.UnitTests;

public class UtilsUnitTest
{
    [Theory]
    [InlineData("à partir de","a-partir-de")]
    [InlineData("AddAnElement", "add-an-element")]
    [InlineData("Awsome", "awsome")]
    public void Sluggify(string input, string output)
    {
        input.Sluggify().Should().Be(output);
    }
}
