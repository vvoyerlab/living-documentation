
namespace LivingDocumentation.UnitTests
{
    public class NamespaceUnitTest
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("    ")]
        public void Namespace_Cannot_Be_Like(string? value)
        {
            Action act = () => { new Namespace() { Value = value }; };
            act.Should().Throw<ArgumentNullException>()
                        .WithParameterName("Namespace should be defined");
        }
    }
}