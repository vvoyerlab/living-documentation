[![.Net](https://img.shields.io/badge/.NET-5C2D91?style=for-the-badge&logo=.net&logoColor=white)](https://learn.microsoft.com/fr-fr/dotnet/csharp/) 
[![C#](https://img.shields.io/badge/csharp-5C2D91?style=for-the-badge&logo=csharp&logoColor=white)](https://learn.microsoft.com/fr-fr/dotnet/csharp/) 
[![MIT License](https://img.shields.io/badge/License-MIT-green.svg)](https://choosealicense.com/licenses/mit/)  

# Living documentation

Inspired by  [Living Documentation by Cyrille MARTRAIRE](https://leanpub.com/livingdocumentation), this application aims to provide a standardized library for automatic documentation generation.

## Project dashboard

Last release : [![Latest Release](https://gitlab.com/vvoyerlab/living-documentation/-/badges/release.svg)](https://gitlab.com/vvoyerlab/living-documentation/-/releases)

| branch | pipeline status | coverage status |
|--|--|--|
| main | [![pipeline status](https://gitlab.com/vvoyerlab/living-documentation/badges/master/pipeline.svg?key_text=master+pipeline&key_width=100)](https://gitlab.com/vvoyerlab/living-documentation/-/commits/master) | ![coverage](https://gitlab.com/vvoyerlab/living-documentation/badges/master/coverage.svg?job=dotnet_test) |
| develop | [![pipeline status](https://gitlab.com/vvoyerlab/living-documentation/badges/develop/pipeline.svg?key_text=develop+pipeline&key_width=100)](https://gitlab.com/vvoyerlab/living-documentation/-/commits/develop) | ![coverage](https://gitlab.com/vvoyerlab/living-documentation/badges/develop/coverage.svg?job=dotnet_test) |

## How to contribute ?

Please check the [Contribution File](https://gitlab.com/vvoyerlab/living-documentation/-/blob/master/CONTRIBUTING.md?ref_type=heads).
