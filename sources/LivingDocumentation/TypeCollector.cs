﻿
namespace LivingDocumentation;

public static class TypeCollector
{
    public static Type[] Collect(Namespace @namespace, IEnumerable<Namespace> exclude)
    {
        IEnumerable<Type> types = AppDomain.CurrentDomain.GetAssemblies()
        .SelectMany(assembly => assembly.GetTypes())
        .Where(type => type.Namespace != null && type.Namespace.StartsWith(@namespace) && !exclude.Contains(type.Namespace));

        return types.ToArray();
    }
}
