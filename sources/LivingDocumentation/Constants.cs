﻿
using System.Reflection;

namespace LivingDocumentation;

internal static class Constants
{
    public const AttributeTargets Types = AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface;

    public const AttributeTargets Member = AttributeTargets.Field | AttributeTargets.Property;

    public const AttributeTargets Code = Types | Member  | AttributeTargets.Method;

    public const BindingFlags VisibleAll = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance;
}
