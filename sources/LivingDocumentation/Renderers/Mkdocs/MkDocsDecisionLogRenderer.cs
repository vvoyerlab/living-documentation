﻿using LivingDocumentation.Features.GenerateDecisionLog;
using System.IO.Abstractions;
using System.Text;

namespace LivingDocumentation.Renderers.Mkdocs;

internal class MkDocsDecisionLogRenderer(string documentationFolder, IFileSystem fileSystem)
{
    private readonly string _documentationFolder = !string.IsNullOrWhiteSpace(documentationFolder) ? documentationFolder : throw new ArgumentNullException(nameof(documentationFolder));    
    private readonly IFileSystem _fileSystem = fileSystem ?? throw new ArgumentNullException(nameof(fileSystem));
    private Dictionary<string,string> _decisions = new Dictionary<string, string>();

    public async Task<ErrorOr<Success>> RenderDecisionLog(LivingDecision[] decisions)
    {        
        string decisionFolder = Path.Combine(_documentationFolder, "decisions");
        if (!_fileSystem.Directory.Exists(decisionFolder))
        {
            _fileSystem.Directory.CreateDirectory(decisionFolder);
        }

        foreach (LivingDecision decision in decisions)
        {
            WriteDecision(decisionFolder, decision);
        }
        WriteIndex(decisionFolder);

        return Result.Success;
    }

    private void WriteDecision(string decisionFolder, LivingDecision decision)
    {
        string decisionName = $"{decision.Title.Sluggify()}.md";
        string decisionFileName = Path.Combine(decisionFolder, decisionName);
        
        StringBuilder decisionContent = new StringBuilder();
        string options = string.Join("\n", decision.ConsideredOptions.Select(x => $"* {x}  "));

        decisionContent.AppendLine($"""
            ---
            title: {decision.Title}
            summary: {decision.Description}
            ---
            ![{decision.Status}](https://img.shields.io/badge/{decision.Status}-blue)
            > Start date : {decision.StartDate}  
            > Release date : {decision.ReleaseDate}  

            ## Context and Problem Statement
            {decision.ProblemsToSolve}

            ## Considered Options
            {options}

            ## Decision Outcome
            {decision.DecisionOutcome}
            {decision.DecisionOutcomeJustification}

            """);

        if (decision.Consequences.Length > 0)
        {
            decisionContent.AppendLine("### Consequences");
            foreach (string consequence in decision.Consequences)
            {
                decisionContent.AppendLine($"* {consequence}  ");
            }
        }

        if (decision.DecisionDrivers.Length > 0)
        {
            decisionContent.AppendLine("## Decision Drivers");
            foreach (string driver in decision.DecisionDrivers)
            {
                decisionContent.AppendLine($"* {driver}  ");
            }
        }

        if (!string.IsNullOrWhiteSpace(decision.Validation))
        {
            decisionContent.AppendLine("## Validation");
            decisionContent.AppendLine(decision.Validation);
        }

        _decisions[decision.Title] = decisionName;
        _fileSystem.File.WriteAllText(decisionFileName, decisionContent.ToString());
    }

    private void WriteIndex(string decisionFolder)
    {
        string pagesFileName = Path.Combine(decisionFolder, ".pages");
        string featureList = string.Join("\n", _decisions.Select(kv => $"  - {kv.Value}  "));
        string nav = $"""
            nav:
            {featureList}
            """;
        _fileSystem.File.WriteAllText(pagesFileName, nav);
    }
}
