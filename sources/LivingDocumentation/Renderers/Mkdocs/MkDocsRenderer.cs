﻿using LivingDocumentation.Features.GenerateDecisionLog;
using LivingDocumentation.Features.GenerateDiagram;
using LivingDocumentation.Features.GenerateFeatures;
using LivingDocumentation.Features.GenerateGlossary;
using LivingDocumentation.Features.GenerateOnboarding;
using LivingDocumentation.Renderers.Mkdocs.DiagramProviders;

namespace LivingDocumentation.Renderers.Mkdocs;

public class MkDocsRenderer(IDocumentationConfiguration configuration, MkDocsConfiguration mkDocsConfiguration)
    : DocumentationRenderer(configuration)
{
    private readonly MkDocsConfiguration _mkDocsConfiguration = mkDocsConfiguration ?? throw new ArgumentNullException(nameof(mkDocsConfiguration));
    private string _documentationFolder = string.Empty;
    protected override async Task<ErrorOr<Success>> Init(Documentation documentation)
    {
        _documentationFolder = Path.Combine(_configuration.DocumentationFolder, "docs");
        if (mkDocsConfiguration.FileSystem.Directory.Exists(_documentationFolder))
        {
            mkDocsConfiguration.FileSystem.Directory.Delete(_documentationFolder, true);
            while (mkDocsConfiguration.FileSystem.Directory.Exists(_documentationFolder)) { }
        }

        mkDocsConfiguration.FileSystem.Directory.CreateDirectory(_documentationFolder);

        string diagramProvider = mkDocsConfiguration.DiagramRenderer switch
        {
            MkDocsConfiguration.DiagramProvider.Mermaid => "mkdocs-mermaid2-plugin>=1.1.1",
            _ => string.Empty
        };
        string diagramPlugin = mkDocsConfiguration.DiagramRenderer switch
        {
            MkDocsConfiguration.DiagramProvider.Mermaid => "- mermaid2",
            _ => string.Empty

        };
        string dependencies = $"""
            mkdocs>=1.5.3
            mkdocs-awesome-pages-plugin>=2.9.2
            mkdocs-material>=9.0.0
            {diagramProvider}
            """;

        string configuration = $"""
            site_name: {_configuration.DocumentationName}
            site_url: {_configuration.DocumentationUrl}
            theme:
              name: material
              palette:
                scheme: slate
                primary: black
                accent: light blue
            nav:
              - index.md
              - ...
            plugins:
              - search
              - awesome-pages
              {diagramPlugin}
            """;
        string indexFileName = Path.Combine(_documentationFolder, "index.md");
        string configurationFile = Path.Combine(_configuration.DocumentationFolder, "mkdocs.yml");
        string dependenciesFile = Path.Combine(_configuration.DocumentationFolder, "requirements.txt");

        _mkDocsConfiguration.FileSystem.File.Copy(_configuration.IndexFile, indexFileName, _mkDocsConfiguration.FileSystem.File.Exists(indexFileName));
        _mkDocsConfiguration.FileSystem.File.WriteAllText(configurationFile, configuration);
        _mkDocsConfiguration.FileSystem.File.WriteAllText(dependenciesFile, dependencies);

        return Result.Success;
    }

    protected override Task<ErrorOr<Success>> RenderDecisionLog(LivingDecision[] decisions)
        => new MkDocsDecisionLogRenderer(_documentationFolder, _mkDocsConfiguration.FileSystem).RenderDecisionLog(decisions);

    protected override Task<ErrorOr<Success>> RenderDiagram(LivingDiagram diagram)
        => mkDocsConfiguration.DiagramRenderer switch
        {
            MkDocsConfiguration.DiagramProvider.Mermaid => new MkdocsMermaidRenderer(_documentationFolder, _mkDocsConfiguration.FileSystem).RenderDiagram(diagram),
            _ => throw new NotImplementedException($"{mkDocsConfiguration.DiagramRenderer} not managed yet.")
        };

    protected override Task<ErrorOr<Success>> RenderFeatures(LivingFeature[] features)
        => new MkDocsFeatureRenderer(_documentationFolder, _mkDocsConfiguration.FileSystem).RenderFeatures(features);

    protected override Task<ErrorOr<Success>> RenderGlossary(LivingGlossay glossary)
        => new MkDocsGlossaryRenderer(_configuration, _documentationFolder, _mkDocsConfiguration.FileSystem).RenderGlossary(glossary);

    protected override Task<ErrorOr<Success>> RenderOnboarding(LivingOnboarding onboarding)
        => new MkDocsOnboardingRenderer(_documentationFolder, _mkDocsConfiguration.FileSystem).RenderOnboarding(onboarding);
}
