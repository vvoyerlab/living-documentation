﻿using System.IO.Abstractions;

namespace LivingDocumentation.Renderers.Mkdocs;

public record MkDocsConfiguration
{
    public IFileSystem FileSystem { get; set; }
    public DiagramProvider DiagramRenderer { get; set; }

    public enum DiagramProvider
    {
        Mermaid
    }
}
