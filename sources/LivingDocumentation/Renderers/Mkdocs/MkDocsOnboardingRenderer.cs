﻿using LivingDocumentation.Features.GenerateOnboarding;
using System.IO.Abstractions;

namespace LivingDocumentation.Renderers.Mkdocs;

internal class MkDocsOnboardingRenderer(string documentationFolder, IFileSystem fileSystem)
{
    private readonly string _documentationFolder = !string.IsNullOrWhiteSpace(documentationFolder) ? documentationFolder : throw new ArgumentNullException(nameof(documentationFolder));
    private readonly IFileSystem _fileSystem = fileSystem ?? throw new ArgumentNullException(nameof(fileSystem));

    public async Task<ErrorOr<Success>> RenderOnboarding(LivingOnboarding onboarding)
    {
        string onboardingFileName = Path.Combine(_documentationFolder, "onboarding.md");
        string onboardingSteps = string.Join("  \n",onboarding.Steps.OrderBy(x => x.Rank).Select(RenderStep));
        string onboardingContent = $"""
            ---
            title: Onboarding
            ---

            {onboardingSteps}

            """;

        _fileSystem.File.WriteAllText(onboardingFileName, onboardingContent);

        return Result.Success;
    }
    private string RenderStep(Step step)
    {
        string path = Path.GetFileName(step.PathToFile);
        return $"""
            ## {step.Rank}. {step.Title}
            {step.Brief}

            > [{path}]({step.PathToFile})

            """;
    }
}
