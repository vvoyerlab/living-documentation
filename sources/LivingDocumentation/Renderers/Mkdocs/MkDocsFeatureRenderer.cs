﻿using LivingDocumentation.Features.GenerateFeatures;
using System.IO.Abstractions;
using System.Text;

namespace LivingDocumentation.Renderers.Mkdocs;

internal class MkDocsFeatureRenderer(string documentationFolder, IFileSystem fileSystem)
{
    private readonly string _documentationFolder = !string.IsNullOrWhiteSpace(documentationFolder) ? documentationFolder : throw new ArgumentNullException(nameof(documentationFolder));
    private readonly IFileSystem _fileSystem = fileSystem ?? throw new ArgumentNullException(nameof(fileSystem));
    private Dictionary<string,string> _writedFeatures = new Dictionary<string, string>();

    public async Task<ErrorOr<Success>> RenderFeatures(LivingFeature[] features)
    {
        string featureFolder = Path.Combine(_documentationFolder, "features");
        if (!_fileSystem.Directory.Exists(featureFolder))
        {
            _fileSystem.Directory.CreateDirectory(featureFolder);
        }

        Dictionary<string, string> _writedFeatures = new Dictionary<string, string>();
        foreach (LivingFeature feature in features)
        {
            WriteFeature(featureFolder, feature);
        }
        WriteIndex(featureFolder);
        return Result.Success;
    }

    private void WriteFeature(string featureFolder, LivingFeature feature)
    {
        string featureName = $"{feature.Name.Sluggify()}.md";
        string featureFileName = Path.Combine(featureFolder, featureName);
        StringBuilder featureContent = new StringBuilder();
        string themeBadge = feature.Theme != null ? $"![Theme](https://img.shields.io/badge/{feature.Theme}-blue)" : "";
        string featureList = string.Join("\n", feature.Links.Select(x => $"* [{x}]({x})"));
        string featureDescription = feature.Description.Replace("\n", "  \n");
        featureContent.AppendLine($"""
            ---
            title: {feature.Name}
            ---
            {themeBadge}
            {featureDescription}

            ## Links
            {featureList}

            """);

        _writedFeatures[feature.Name] = featureName;
        _fileSystem.File.WriteAllText(featureFileName, featureContent.ToString());
    }

    private void WriteIndex(string featureFolder)
    {
        string pagesFileName = Path.Combine(featureFolder, ".pages");
        string featureList = string.Join("\n", _writedFeatures.Select(kv => $"  - {kv.Value}"));
        string nav = $"""
            nav:
            {featureList}
            """;
        _fileSystem.File.WriteAllText(pagesFileName, nav);
    }
}
