﻿using LivingDocumentation.Features.GenerateGlossary;
using System.IO.Abstractions;

namespace LivingDocumentation.Renderers.Mkdocs;

internal class MkDocsGlossaryRenderer(IDocumentationConfiguration configuration, string documentationFolder, IFileSystem fileSystem)
{
    private readonly IDocumentationConfiguration _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
    private readonly string _documentationFolder = !string.IsNullOrWhiteSpace(documentationFolder) ? documentationFolder : throw new ArgumentNullException(nameof(documentationFolder));
    private readonly IFileSystem _fileSystem = fileSystem ?? throw new ArgumentNullException(nameof(fileSystem));

    public async Task<ErrorOr<Success>> RenderGlossary(LivingGlossay glossary)
    {
        string glossaryFileName = Path.Combine(_documentationFolder, "glossary.md");
        string words = string.Join("\n",glossary.Words.Select(x => $"* **{x.Name}** : {x.Definition}"));
        string glossaryContent = $"""
            ---
            title: {_configuration.GlossaryName}
            ---

            {words}
            """;

        _fileSystem.File.WriteAllText(glossaryFileName, glossaryContent);

        return Result.Success;
    }
}
