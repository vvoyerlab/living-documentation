﻿using LivingDocumentation.Features.GenerateDiagram;
using System.IO.Abstractions;

namespace LivingDocumentation.Renderers.Mkdocs.DiagramProviders;

public class MkdocsMermaidRenderer(string documentationFolder, IFileSystem fileSystem)
{
    private readonly string _documentationFolder = !string.IsNullOrWhiteSpace(documentationFolder) ? documentationFolder : throw new ArgumentNullException(nameof(documentationFolder));
    private readonly IFileSystem _fileSystem = fileSystem ?? throw new ArgumentNullException(nameof(fileSystem));

    public async Task<ErrorOr<Success>> RenderDiagram(LivingDiagram diagram)
    {
        string diagramFileName = Path.Combine(_documentationFolder, "diagram.md");
        string elements = string.Join("\n", diagram.Elements.Select(RenderElement));
        string relationships = string.Join("\n", diagram.Relationships.Select(RenderRelationship));
        string diagramContent = $"""
            ---
            title: Diagram
            ---

            ```mermaid
            classDiagram
            {elements}

            {relationships}
            ```
            """;

        _fileSystem.File.WriteAllText(diagramFileName, diagramContent);

        return Result.Success;
    }

    public string RenderElement(Element element)
    {
        string type = element.Type switch
        {
            ElementType.Interface => "<<Interface>>",
            ElementType.Enum => "<<Enum>>",
            ElementType.Struct => "<<Struct>>",
            _ => string.Empty,
        };
        string template = type != string.Empty ? """
            class %NAME% {
              %TYPE%
            }
            """ :
            """
            class %NAME%
            """;
        return template.Replace("%NAME%", element.Name)
                       .Replace("%TYPE%", type);
    }
    public string RenderRelationship(Relationship relationship)
    {
        string relationLink = relationship.Type switch
        {
            RelationshipType.Implements => "..|>",
            RelationshipType.Extends => "--|>",
            RelationshipType.RelatedTo => "-->",
            _ => "..>"
        };
        string relationName = string.IsNullOrWhiteSpace(relationship.Name) ? string.Empty : $": {relationship.Name}";
        return $"""
            {relationship.ElementNameFrom} {relationLink} {relationship.ElementNameTo} {relationName}
            """;
    }
}
