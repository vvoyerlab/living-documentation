﻿using LivingDocumentation.Features.GenerateDecisionLog;
using LivingDocumentation.Features.GenerateDiagram;
using LivingDocumentation.Features.GenerateFeatures;
using LivingDocumentation.Features.GenerateGlossary;
using LivingDocumentation.Features.GenerateOnboarding;

namespace LivingDocumentation.Renderers;

[AnyDecisionRecord(
    Title="Add renderer into the lib",
    Description = """
    Make easy to render documentation generate by the lib.
    """,
    Status = AnyDecisionRecord.DecisionStatus.Accepted,
    ProblemsToSolve = """
    Render documentation generate by the package.
    """,
    ConsideredOptions = [
        "No code for that : User make there own code to manage results.",
        "Template system : A system that takes templates as a parameter for each type of doc and integrates the result within.",
        "Integrated toolkit : Pre-made class set to render documentation."
        ],
    DecisionOutcome = """
    **Integrated toolkit**
    This option was chosen because it allows a plug and play mode of the package. To be seen in use if we keep this or not.
    """
    )]
public abstract class DocumentationRenderer(IDocumentationConfiguration configuration)
{
    protected readonly IDocumentationConfiguration _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));

    public async Task<ErrorOr<Success>> Render(Documentation documentation)
    {
        List<Error> errors = new List<Error>();

        ErrorOr<Success> initResult = await Init(documentation);
        if (initResult.IsError) { errors.AddRange(initResult.Errors); }

        if (documentation.Glossary != null)
        {
            ErrorOr<Success> glossaryRenderingResult = await RenderGlossary(documentation.Glossary);
            if (glossaryRenderingResult.IsError) { errors.AddRange(glossaryRenderingResult.Errors); }
        }
        if (documentation.Features != null && documentation.Features.Length > 0)
        {
            ErrorOr<Success> featuresRenderingResult = await RenderFeatures(documentation.Features);
            if (featuresRenderingResult.IsError) { errors.AddRange(featuresRenderingResult.Errors); }
        }
        if (documentation.Onboarding != null)
        {
            ErrorOr<Success> onboardingRenderingResult = await RenderOnboarding(documentation.Onboarding);
            if (onboardingRenderingResult.IsError) { errors.AddRange(onboardingRenderingResult.Errors); }
        }
        if (documentation.Diagram != null)
        {
            ErrorOr<Success> diagramRenderingResult = await RenderDiagram(documentation.Diagram);
            if (diagramRenderingResult.IsError) { errors.AddRange(diagramRenderingResult.Errors); }
        }
        if (documentation.Decisions != null && documentation.Decisions.Length > 0)
        {
            ErrorOr<Success> decisionsRenderingResult = await RenderDecisionLog(documentation.Decisions);
            if (decisionsRenderingResult.IsError) { errors.AddRange(decisionsRenderingResult.Errors); }
        }

        if (errors.Count > 0)
        {
            errors.Insert(0, Error.Validation(
                code: "documentation.rendering",
                description: "Rendering not complete successfully"
                ));
            return errors;
        }

        return Result.Success;
    }
    protected abstract Task<ErrorOr<Success>> Init(Documentation documentation);
    protected abstract Task<ErrorOr<Success>> RenderGlossary(LivingGlossay glossary);
    protected abstract Task<ErrorOr<Success>> RenderFeatures(LivingFeature[] features);
    protected abstract Task<ErrorOr<Success>> RenderOnboarding(LivingOnboarding onboarding);
    protected abstract Task<ErrorOr<Success>> RenderDiagram(LivingDiagram diagram);
    protected abstract Task<ErrorOr<Success>> RenderDecisionLog(LivingDecision[] decisions);
}
