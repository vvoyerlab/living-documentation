﻿using LivingDocumentation.Features.GenerateDecisionLog;
using LivingDocumentation.Features.GenerateDiagram;
using LivingDocumentation.Features.GenerateFeatures;
using LivingDocumentation.Features.GenerateGlossary;
using LivingDocumentation.Features.GenerateOnboarding;

namespace LivingDocumentation.Renderers;

public record Documentation
{
    public LivingGlossay Glossary { get; set; }
    public LivingFeature[] Features { get; set; }
    public LivingOnboarding Onboarding { get; set; }
    public LivingDiagram Diagram { get; set; }
    public LivingDecision[] Decisions { get; set; }
}