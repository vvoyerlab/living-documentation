﻿
namespace LivingDocumentation.Features.GenerateFeatures;

public record LivingFeature(string Name, string Description, string[] Links, string? Theme);
