﻿
namespace LivingDocumentation.Features.GenerateFeatures;

/// <summary>
/// Generate features descriptions.
/// </summary>
[Feature(
    Name = "Generate Features",
    QuickBrief = """
    As developper
    I want features descriptions of application
    So that I have all actions allowed by application
    """
    )]
public interface IGenerateFeatures
{
    public ErrorOr<LivingFeature[]> Execute(Namespace @namespace);

}