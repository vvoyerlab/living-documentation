﻿
namespace LivingDocumentation.Features.GenerateFeatures;

public sealed class LivingFeatureGenerator(IDocumentationConfiguration configuration)
    : DocumentationGenerator(configuration), IGenerateFeatures
{
    public ErrorOr<LivingFeature[]> Execute(Namespace @namespace)
    {
        List<LivingFeature> livingFeatures = new List<LivingFeature>();

        AnnotationCollector.Collect(@namespace, _configuration.ExcludedNamespaces, out Feature[] features);

        if (features.Length <= 0)
        {
            return Error.NotFound(
                code: "features",
                description: "No feature found",
                metadata: new Dictionary<string, object>() { { "namespace", @namespace } }
                );
        }

        foreach (Feature feature in features) 
        {
            livingFeatures.Add(new(
                Name: feature.Name,
                Description: feature.QuickBrief,
                Links: feature.Links,
                Theme: feature.Theme
                ));
        }

        return livingFeatures.ToArray();
    }
}