﻿
using System.Reflection;
using System.Text.RegularExpressions;

namespace LivingDocumentation.Features.GenerateDiagram;

public sealed class LivingDiagramGenerator(IDocumentationConfiguration configuration)
    : DocumentationGenerator(configuration), IGenerateDiagram
{
    private List<Element> _elements = new List<Element>();
    private List<Relationship> _relationships = new List<Relationship>();

    public ErrorOr<LivingDiagram> Execute(Namespace @namespace)
    {
        _elements = new List<Element>();
        _relationships = new List<Relationship>();
        Regex validName = new Regex("^[A-Za-z0-9]{1,}$");
        IEnumerable<Type> types = TypeCollector.Collect(@namespace, _configuration.ExcludedNamespaces).Where(t => validName.IsMatch(t.Name));

        foreach (Type type in types)
        {
            AddElement(type, @namespace);
            AddRelations(type, @namespace);
        }

        return new LivingDiagram([.. _elements], [.. _relationships]);
    }

    private void AddElement(Type type, Namespace @namespace)
    {
        _elements.Add(new Element(
            Name: type.Name,
            Namespace: type.Namespace ?? "",
            Type: MapToElementType(type)
            ));
        if (type.BaseType != null && type.BaseType != typeof(object) && !type.IsEnum && !type.IsPrimitive)
        {
            _relationships.Add(new Relationship
            {
                ElementNameFrom = type.Name,
                ElementNameTo = type.BaseType.Name,
                Type = RelationshipType.Extends
            });
        }
        foreach (var interfaceType in type.GetInterfaces())
        {
            if (!interfaceType.IsEnum && !interfaceType.IsPrimitive && interfaceType.Namespace is not null && interfaceType.Namespace.Contains(@namespace))
            {
                _relationships.Add(new Relationship
                {
                    ElementNameFrom = type.Name,
                    ElementNameTo = interfaceType.Name,
                    Type = RelationshipType.Implements
                });
            }
        }
    }
    private void AddRelations(Type type, Namespace @namespace)
    {
        foreach (FieldInfo field in type.GetFields(Constants.VisibleAll))
        {
            Type fieldType = field.GetType();
            if (!fieldType.IsPrimitive && fieldType.Namespace != null && fieldType.Namespace.StartsWith(@namespace))
            {
                _relationships.Add(
                    new Relationship
                    {
                        ElementNameFrom = type.Name,
                        ElementNameTo = fieldType.Name,
                        Type = RelationshipType.RelatedTo,
                        Name = field.Name
                    });
            }
        }
        foreach (PropertyInfo property in type.GetProperties(Constants.VisibleAll))
        {
            Type propertyType = property.GetType();
            if (!propertyType.IsPrimitive && propertyType.Namespace != null && propertyType.Namespace.StartsWith(@namespace))
            {
                _relationships.Add(
                    new Relationship
                    {
                        ElementNameFrom = type.Name,
                        ElementNameTo = propertyType.Name,
                        Type = RelationshipType.RelatedTo,
                        Name = property.Name
                    });
            }
        }
    }
    private static ElementType MapToElementType(Type type)
    {
        if (type.IsClass) return ElementType.Class;
        if (type.IsInterface) return ElementType.Interface;
        if (type.IsEnum) return ElementType.Enum;
        if (type.IsValueType && !type.IsEnum && !type.IsPrimitive) return ElementType.Struct;

        return ElementType.Unknown;
    }
}