﻿
namespace LivingDocumentation.Features.GenerateDiagram;

public record Relationship
{
    public string ElementNameFrom { get; set; }
    public string ElementNameTo { get; set; }
    public RelationshipType Type { get; set; }
    public string Name { get; set; } = string.Empty;
}
