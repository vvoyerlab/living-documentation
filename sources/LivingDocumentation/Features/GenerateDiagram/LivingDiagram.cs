﻿
namespace LivingDocumentation.Features.GenerateDiagram;

public record LivingDiagram(Element[] Elements, Relationship[] Relationships);
