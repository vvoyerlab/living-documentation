﻿
namespace LivingDocumentation.Features.GenerateDiagram;

public enum ElementType
{
    Class,
    Struct,
    Interface,
    Enum,
    Unknown
}
