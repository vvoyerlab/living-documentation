﻿namespace LivingDocumentation.Features.GenerateDiagram;

/// <summary>
/// Generate a diagram of a namespace.
/// </summary>
[Feature(
    Name = "Generate Diagram",
    QuickBrief = """
    As developer
    I Want a diagram of namespace
    So that I have a map of elements and there relations
    """
    )]
public interface IGenerateDiagram
{
    public ErrorOr<LivingDiagram> Execute(Namespace @namespace);
}