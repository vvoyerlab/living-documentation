﻿
namespace LivingDocumentation.Features.GenerateDiagram;

public record Element(string Name, string Namespace, ElementType Type);
