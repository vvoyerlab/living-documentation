﻿
namespace LivingDocumentation.Features.GenerateDiagram;

public enum RelationshipType
{
    RelatedTo,
    Implements,
    Extends
}