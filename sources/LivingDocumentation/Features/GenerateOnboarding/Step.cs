﻿namespace LivingDocumentation.Features.GenerateOnboarding;

public record Step(int Rank, string Title, string Brief, string PathToFile);