﻿namespace LivingDocumentation.Features.GenerateOnboarding;

public record LivingOnboarding(Step[] Steps);
