﻿namespace LivingDocumentation.Features.GenerateOnboarding;

/// <summary>
/// Generate Onbaording sheet.
/// </summary>
[Feature(
    Name = "Generate Onboarding",
    QuickBrief = """
    As developer
    I Want an onboarding sheet
    So that new developers will be able to better integrate the project
    """
    )]
public interface IGenerateOnboarding
{
    public ErrorOr<LivingOnboarding> Execute(Namespace @namespace);
}
