﻿using System.Reflection;

namespace LivingDocumentation.Features.GenerateOnboarding;

public sealed class LivingOnboardingGenerator(IDocumentationConfiguration configuration)
    : DocumentationGenerator(configuration), IGenerateOnboarding
{
    public ErrorOr<LivingOnboarding> Execute(Namespace @namespace)
    {
        List<Step> steps = new List<Step>();

        AnnotationCollector.CollectFromTypes(@namespace, _configuration.ExcludedNamespaces, out Dictionary<Type, OnBoarding> onboardingOnTypes);
        foreach (Type type in onboardingOnTypes.Keys)
        {
            OnBoarding onBoarding = onboardingOnTypes[type];
            steps.Add(
                new Step(
                    Rank: onBoarding.Step,
                    Title: onBoarding.Title,
                    Brief: onBoarding.QuickBrief,
                    PathToFile: CalculatePathToFile(type)
                    )
                );
        }

        AnnotationCollector.CollectFromMethods(@namespace, _configuration.ExcludedNamespaces, out Dictionary<MethodInfo, OnBoarding> onboardingOnMethods);
        foreach (MethodInfo method in onboardingOnMethods.Keys)
        {
            OnBoarding onBoarding = onboardingOnMethods[method];
            steps.Add(
                new Step(
                    Rank: onBoarding.Step,
                    Title: onBoarding.Title,
                    Brief: onBoarding.QuickBrief,
                    PathToFile: method.DeclaringType is null ? string.Empty : CalculatePathToFile(method.DeclaringType)
                    )
                );
        }

        AnnotationCollector.CollectFromMembers(@namespace, _configuration.ExcludedNamespaces, out Dictionary<MemberInfo, OnBoarding> onboardingOnMembers);
        foreach (MemberInfo member in onboardingOnMembers.Keys)
        {
            OnBoarding onBoarding = onboardingOnMembers[member];
            steps.Add(
                new Step(
                    Rank: onBoarding.Step,
                    Title: onBoarding.Title,
                    Brief: onBoarding.QuickBrief,
                    PathToFile: member.DeclaringType is null ? string.Empty : CalculatePathToFile(member.DeclaringType)
                    )
                );
        }

        return new LivingOnboarding(steps.ToArray());
    }

    private string CalculatePathToFile(Type type)
    {
        if (string.IsNullOrWhiteSpace(type.Name))
            return string.Empty;

        string assemblyName = type.Assembly.GetName().Name;
        string classFile = $"{type.FullName.Replace($"{assemblyName}.", "").Replace('.', Path.DirectorySeparatorChar)}.cs";

        return Path.Combine(_configuration.SourceRootPath, assemblyName, classFile);
    }
}
