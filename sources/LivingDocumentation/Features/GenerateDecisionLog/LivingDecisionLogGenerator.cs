﻿
namespace LivingDocumentation.Features.GenerateDecisionLog;

public sealed class LivingDecisionLogGenerator(IDocumentationConfiguration configuration)
    : DocumentationGenerator(configuration), IGenerateDecisionLog
{
    public ErrorOr<LivingDecision[]> Execute(Namespace @namespace)
    {
        List<LivingDecision> decisions = new List<LivingDecision>();

        AnnotationCollector.Collect(@namespace, _configuration.ExcludedNamespaces, out AnyDecisionRecord[] decisionRecords);
        if (decisionRecords.Length <= 0)
        {
            return Error.NotFound(
                code: "decisionLog",
                description: "No decision found",
                metadata: new Dictionary<string, object>() { { "namespace", @namespace } }
                );
        }

        foreach (AnyDecisionRecord record in decisionRecords)
        {
            decisions.Add(
                new LivingDecision
                {
                    Title = record.Title,
                    Status = record.Status.ToString(),
                    StartDate = record.StartDate,
                    ReleaseDate = record.ReleaseDate,
                    Description = record.Description,
                    ProblemsToSolve = record.ProblemsToSolve,
                    DecisionDrivers = record.DecisionDrivers,
                    ConsideredOptions = record.ConsideredOptions,
                    DecisionOutcome = record.DecisionOutcome,
                    DecisionOutcomeJustification = record.DecisionOutcomeJustification,
                    Consequences = record.Consequences,
                    Validation = record.Validation,
                });
        }

        return decisions.ToArray();
    }
}
