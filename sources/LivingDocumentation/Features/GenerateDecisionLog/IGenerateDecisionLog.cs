﻿
namespace LivingDocumentation.Features.GenerateDecisionLog;

/// <summary>
/// Generate desicion log.
/// </summary>
[Feature(
    Name = "Generate decision log",
    QuickBrief = """
    As develop
    I Want a decision log
    So that I can track all decision make on the codebase
    """
    )]
public interface IGenerateDecisionLog
{
    public ErrorOr<LivingDecision[]> Execute(Namespace @namespace);
}