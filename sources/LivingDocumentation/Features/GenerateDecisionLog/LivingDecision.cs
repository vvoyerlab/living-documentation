﻿
namespace LivingDocumentation.Features.GenerateDecisionLog;

public record LivingDecision
{
    public string Title { get; set; } = string.Empty;
    public string Status { get; set; } = string.Empty;
    public string StartDate { get; set; } = string.Empty;
    public string ReleaseDate { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public string ProblemsToSolve { get; set; } = string.Empty;
    public string[] DecisionDrivers { get; set; } = Array.Empty<string>();
    public string[] ConsideredOptions { get; set; } = Array.Empty<string>();
    public string DecisionOutcome { get; set; } = string.Empty;
    public string DecisionOutcomeJustification { get; set; } = string.Empty;
    public string[] Consequences { get; set; } = Array.Empty<string>();
    public string Validation { get; set; } = string.Empty;
}
