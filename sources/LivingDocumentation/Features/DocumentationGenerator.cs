﻿
namespace LivingDocumentation.Features;

public abstract class DocumentationGenerator(IDocumentationConfiguration configuration)
{
    protected readonly IDocumentationConfiguration _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
}
