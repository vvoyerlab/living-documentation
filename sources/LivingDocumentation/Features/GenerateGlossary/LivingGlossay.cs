﻿namespace LivingDocumentation.Features.GenerateGlossary;

public record LivingGlossay (Word[] Words);
