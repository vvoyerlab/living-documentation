﻿
namespace LivingDocumentation.Features.GenerateGlossary;

public sealed class LivingGlossaryGenerator(IDocumentationConfiguration configuration)
    : DocumentationGenerator(configuration), IGenerateGlossary
{
    public ErrorOr<LivingGlossay> Execute(Namespace @namespace)
    {
        List<Word> words = new List<Word>();

        AnnotationCollector.Collect(@namespace, _configuration.ExcludedNamespaces, out Glossary[] glossary);

        if (glossary.Length <= 0)
        {
            return Error.NotFound(
                code: "glossary",
                description: "No glossary found",
                metadata: new Dictionary<string, object>() { { "namespace", @namespace } }
                );
        }

        foreach (Glossary word in  glossary)
        {
            words.Add(new(word.Name, word.Definition));
        }

        return new LivingGlossay(words.ToArray());
    }
}
