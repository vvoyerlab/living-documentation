﻿
namespace LivingDocumentation.Features.GenerateGlossary;

/// <summary>
/// Generate glossary of application.
/// </summary>
[Feature(
    Name = "Generate Glossary",
    QuickBrief = """
    As developer
    I want a glossay
    So that I have the vocabulary used in the application
    """
    )]
public interface IGenerateGlossary
{
    public ErrorOr<LivingGlossay> Execute(Namespace @namespace);
}