﻿namespace LivingDocumentation.Features.GenerateGlossary;

public record Word(string Name, string Definition);