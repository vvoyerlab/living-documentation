﻿namespace LivingDocumentation;

public interface IDocumentationConfiguration
{
    public string DocumentationFolder { get; }
    public string ProjectRelativePathFromOutputPath { get; }
    public string SourceRootPath { get; }
    public string ProjectRootPath { get; }
    public IEnumerable<Namespace> ExcludedNamespaces { get; }
    public string IndexFile { get; }
    public string DocumentationName { get; }
    public string DocumentationUrl { get; }
    public string GlossaryName { get; }
}
