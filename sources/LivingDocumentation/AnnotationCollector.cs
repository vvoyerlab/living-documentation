﻿using System.Reflection;

namespace LivingDocumentation;

public static class AnnotationCollector
{
    public static void Collect<T>(Namespace @namespace, IEnumerable<Namespace> exclude, out T[] items)
        where T : Attribute
    {
        List<T> collectedItems = new List<T>();
        
        IEnumerable<Type> types = TypeCollector.Collect(@namespace, exclude);

        foreach (Type type in types)
        {
            T? attributeOnType = type.GetCustomAttribute<T>();
            if (attributeOnType != null)
                collectedItems.Add(attributeOnType);

            foreach (MethodInfo method in type.GetMethods(Constants.VisibleAll))
            {
                T? attributeOnMethod = method.GetCustomAttribute<T>();
                if (attributeOnMethod != null)
                    collectedItems.Add(attributeOnMethod);
            }
            foreach (MemberInfo member in type.GetMembers(Constants.VisibleAll))
            {
                T? attributeOnMember = member.GetCustomAttribute<T>();
                if (attributeOnMember != null)
                    collectedItems.Add(attributeOnMember);
            }
        }

        items = collectedItems.ToArray();
    }

    public static void CollectFromTypes<T>(Namespace @namespace, IEnumerable<Namespace> exclude, out Dictionary<Type,T> items) 
        where T : Attribute
    {
        IEnumerable<Type> types = AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(assembly => assembly.GetTypes())
            .Where(type => type.Namespace != null && type.Namespace.StartsWith(@namespace) && !exclude.Contains(type.Namespace) && type.GetCustomAttribute<T>() != null);

        items = new Dictionary<Type,T>();

        foreach (Type type in types)
        {
            items[type] = type.GetCustomAttribute<T>()!;
        }
    }

    public static void CollectFromMethods<T>(Namespace @namespace, IEnumerable<Namespace> exclude, out Dictionary<MethodInfo, T> items)
        where T : Attribute
    {
        IEnumerable<MethodInfo> methods = AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(assembly => assembly.GetTypes())
            .Where(type => type.Namespace != null && type.Namespace.StartsWith(@namespace) && !exclude.Contains(type.Namespace))
            .SelectMany(type => type.GetMethods(Constants.VisibleAll))
            .Where(method => method.GetCustomAttribute<T>() != null);

        items = new Dictionary<MethodInfo, T>();

        foreach (MethodInfo method in methods)
        {
            items[method] = method.GetCustomAttribute<T>()!;
        }
    }

    public static void CollectFromMembers<T>(Namespace @namespace, IEnumerable<Namespace> exclude, out Dictionary<MemberInfo, T> items)
        where T : Attribute
    {
        IEnumerable<MemberInfo> members = AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(assembly => assembly.GetTypes())
            .Where(type => type.Namespace != null && type.Namespace.StartsWith(@namespace) && !exclude.Contains(type.Namespace))
            .SelectMany(type => type.GetMembers(Constants.VisibleAll))
            .Where(member => member.GetCustomAttribute<T>() != null);

        items = new Dictionary<MemberInfo, T>();

        foreach (MemberInfo member in members)
        {
            items[member] = member.GetCustomAttribute<T>()!;
        }
    }
}
