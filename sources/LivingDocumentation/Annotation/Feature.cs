﻿namespace LivingDocumentation.Annotation;

/// <summary>
/// Mark this interface as a feature.
/// </summary>
[AttributeUsage(AttributeTargets.Interface)]
[Glossary(
    Name = "Feature",
    Definition = "Programming interface of the codebase."
    )]
public class Feature : Attribute
{
    public string Name { get; set; } = string.Empty;
    public string QuickBrief { get; set; } = string.Empty;
    public string? Theme { get; set; } = null;
    public string[] Links { get; set; } = Array.Empty<string>();    
}
