﻿namespace LivingDocumentation.Annotation;

/// <summary>
/// Step of onboarding.
/// </summary>
[AttributeUsage(Constants.Code)]
[Glossary(
    Name = "OnBoarding",
    Definition = "Step of process of integrating and familiarizing new hires on codebase."
    )]
public class OnBoarding : Attribute
{
    public int Step { get; set; } = 1;
    public string Title { get; set; } = string.Empty;
    public string QuickBrief { get; set; } = string.Empty;
}
