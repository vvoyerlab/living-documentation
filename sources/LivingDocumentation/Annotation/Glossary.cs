﻿namespace LivingDocumentation.Annotation;

/// <summary>
/// Business word.
/// </summary>
[AttributeUsage(Constants.Types)]
[Glossary(
    Name = "Glossary",
    Definition = "Business word."
    )]
public class Glossary : Attribute
{
    public string Name { get; set; } = string.Empty;
    public string Definition { get; set; } = string.Empty;
}
