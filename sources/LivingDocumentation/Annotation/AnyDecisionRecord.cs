﻿
namespace LivingDocumentation.Annotation;

/// <summary>
/// Capturing architectural decisions and allowing to capture any decisions taken.
/// </summary>
[AttributeUsage(Constants.Code, AllowMultiple = true)]
[Glossary(
    Name = "AnyDecisionRecord",
    Definition = "Capturing architectural decisions and allowing to capture any decisions taken."
    )]
public class AnyDecisionRecord : Attribute
{
    public string Title { get; set; } = string.Empty;
    public DecisionStatus Status { get; set; } = DecisionStatus.Proposed;
    public string StartDate { get; set; } = string.Empty;
    public string ReleaseDate { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public string ProblemsToSolve { get; set; } = string.Empty;
    public string[] DecisionDrivers { get; set; } = Array.Empty<string>();
    public string[] ConsideredOptions { get; set; } = Array.Empty<string>();
    public string DecisionOutcome { get; set; } = string.Empty;
    public string DecisionOutcomeJustification { get; set; } = string.Empty;
    public string[] Consequences { get; set; } = Array.Empty<string>();
    public string Validation { get; set;} = string.Empty;    

    public enum DecisionStatus
    {
        Proposed,
        Rejected,
        Accepted,
        Deprecated
    }
}
