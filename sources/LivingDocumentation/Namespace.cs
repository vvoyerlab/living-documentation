﻿
namespace LivingDocumentation;

public readonly record struct Namespace
{
    public string Value
    {
        get
        {
            return _value;
        }
        init
        {
            if (string.IsNullOrWhiteSpace(value))
                throw new ArgumentNullException("Namespace should be defined");
            _value = value;
        }
    }
    private readonly string _value;
    private Namespace(string value)
    {
        Value = value;
    }

    public static implicit operator string(Namespace @namespace) => @namespace.Value;
    public static implicit operator Namespace(string value) => new Namespace(value);
}
