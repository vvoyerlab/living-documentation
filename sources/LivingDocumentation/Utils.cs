﻿using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace LivingDocumentation;

public static class Utils
{
    public static string Sluggify(this string value)
    {
        var stringWithSpaces = Regex.Replace(value, "(?<!^)([A-Z])", " $1");
        string normalizedString = stringWithSpaces.Normalize(NormalizationForm.FormD);
        StringBuilder stringBuilder = new StringBuilder();

        foreach (char c in normalizedString)
        {
            UnicodeCategory unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
            if (unicodeCategory != UnicodeCategory.NonSpacingMark)
            {
                stringBuilder.Append(c);
            }
        }

        string slug = stringBuilder
            .ToString()
            .Normalize(NormalizationForm.FormC)
            .ToLowerInvariant();

        slug = Regex.Replace(slug, @"\s", "-");
        slug = Regex.Replace(slug, @"[^a-z0-9\-]", string.Empty);
        slug = Regex.Replace(slug, @"-+", "-");

        return slug;
    }    
}
