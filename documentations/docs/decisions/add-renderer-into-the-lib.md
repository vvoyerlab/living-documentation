---
title: Add renderer into the lib
summary: Make easy to render documentation generate by the lib.
---
![Accepted](https://img.shields.io/badge/Accepted-blue)
> Start date :   
> Release date :   

## Context and Problem Statement
Render documentation generate by the package.

## Considered Options
* No code for that : User make there own code to manage results.  
* Template system : A system that takes templates as a parameter for each type of doc and integrates the result within.  
* Integrated toolkit : Pre-made class set to render documentation.  

## Decision Outcome
**Integrated toolkit**
This option was chosen because it allows a plug and play mode of the package. To be seen in use if we keep this or not.


