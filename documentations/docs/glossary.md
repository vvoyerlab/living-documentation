---
title: Annotations
---

* **AnyDecisionRecord** : Capturing architectural decisions and allowing to capture any decisions taken.
* **Feature** : Programming interface of the codebase.
* **Glossary** : Business word.
* **OnBoarding** : Step of process of integrating and familiarizing new hires on codebase.