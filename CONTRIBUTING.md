Hello,

We are delighted to see your enthusiasm to participate in our project. Here are the steps to contribute:

1. Create an issue using the appropriate template. Issue templates are [here](.gitlab/issue_templates)
2. Fork the project.
3. Make your changes.
4. Test your changes.
5. Submit a merge request using the [review template](.gitlab/merge_request_templates/review.md).
6. Wait for code review.
